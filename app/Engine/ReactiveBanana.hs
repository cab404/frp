module Engine.ReactiveBanana
  ( eventHandler
  , timer
  , allocate

  , setWorkerInput
  , setWorkerInputWith
  , setWorkerOutput
  , setWorkerOutputWith
  ) where

import RIO.Local

import Engine.Worker qualified as Worker
import Reactive.Banana qualified as RB
import Reactive.Banana.Frameworks qualified as RBF
import Resource.Region qualified as Region
import UnliftIO.Resource qualified as Resource

eventHandler
  :: (Resource.MonadResource m, MonadIO io)
  => ((a -> io ()) -> m Resource.ReleaseKey)
  -> ResourceT m (RBF.MomentIO (RB.Event a))
eventHandler action = do
  (addHandler, fire) <- liftIO RBF.newAddHandler
  Region.local_ $ action (liftIO . fire)
  pure $ RBF.fromAddHandler addHandler

timer
  :: MonadUnliftIO m
  => Int
  -> ResourceT m (RBF.MomentIO (RB.Event Double))
timer delayMS = do
  (addHandler, fire) <- liftIO RBF.newAddHandler
  ticker <- async $ forever do
    threadDelay delayMS
    getMonotonicTime >>= liftIO . fire
  Region.attachAsync ticker
  pure $ RBF.fromAddHandler addHandler

allocate
  :: MonadUnliftIO m
  => Bool
  -> (UnliftIO m -> RBF.MomentIO ())
  -> ResourceT m RBF.EventNetwork
allocate startActuated builder = do
  unlift <- lift askUnliftIO
  fmap snd $
    Resource.allocate
      do
        network <- RBF.compile $ builder unlift
        when startActuated do
          RBF.actuate network
        pure network
      RBF.pause

setWorkerInput
  :: Worker.HasInput var
  => var
  -> RB.Event (Worker.GetInput var)
  -> RBF.MomentIO ()
setWorkerInput p = RBF.reactimate . fmap (Worker.pushInput p . const)

setWorkerInputWith
  :: Worker.HasInput var
  => var
  -> RB.Event a
  -> (a -> Worker.GetInput var)
  -> RBF.MomentIO ()
setWorkerInputWith p e f = setWorkerInput p (fmap f e)

setWorkerOutput
  :: Worker.HasOutput var
  => var
  -> RB.Event (Worker.GetOutput var)
  -> RBF.MomentIO ()
setWorkerOutput p = RBF.reactimate . fmap (Worker.pushOutput p . const)

setWorkerOutputWith
  :: Worker.HasOutput var
  => var
  -> RB.Event a
  -> (a -> Worker.GetOutput var)
  -> RBF.MomentIO ()
setWorkerOutputWith p e f = setWorkerOutput p (fmap f e)
