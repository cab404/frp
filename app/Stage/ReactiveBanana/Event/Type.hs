module Stage.ReactiveBanana.Event.Type
  ( Event(..)
  ) where

import RIO

import Engine.Camera.Event.Type qualified as Camera

data Event
  = DoNothing
  | Camera Camera.Event
  deriving (Show)
