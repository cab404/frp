module Stage.ReactiveBanana.World.Scene where

import RIO.Local

import Engine.Camera qualified as Camera
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 (Scene(..), emptyScene)
import Geomancy.Transform qualified as Transform

-- type InputVar = Worker.Var Input

type Input = Camera.ProjectionInput 'Camera.Perspective

-- initialInput :: Input
-- initialInput = Input

type Process = Worker.Merge Scene

mkScene :: Camera.Projection 'Camera.Perspective -> Camera.View -> Input -> Scene
mkScene Camera.Projection{..} Camera.View{..} Camera.ProjectionInput{..} =
  emptyScene
    { sceneProjection    = projectionTransform
    , sceneInvProjection = Transform.inverse projectionTransform -- projectionInverse

    , sceneView          = viewTransform
    , sceneInvView       = viewTransformInv
    , sceneViewPos       = viewPosition
    , sceneViewDir       = viewDirection

    , sceneTweaks        = vec4 projectionNear projectionFar 0 0
    }
