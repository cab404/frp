module Stage.ReactiveBanana.Types
  ( Stage
  , FrameResources(..)
  , RunState(..)
  ) where

import RIO.Local

import Engine.Camera qualified as Camera
import Engine.Camera.Controls qualified as CameraControls
import Engine.Events qualified as Events
import Engine.Worker qualified as Worker
import Reactive.Banana.Frameworks (EventNetwork)
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import RIO.Vector.Storable qualified as Storable

import Stage.ReactiveBanana.Event.Type (Event)
import Stage.ReactiveBanana.World.Mesh qualified as Mesh
import Data.StateVar (StateVar)

type Stage = Basic.Stage FrameResources RunState

data FrameResources = FrameResources
  { frScene :: Set0.FrameResource '[Set0.Scene]

  , frMeshInstances :: Mesh.Observer
  , frPlaneCursor   :: Mesh.Observer
  }

data RunState = RunState
  { rsEvents      :: Maybe (Events.Sink Event RunState)

  , rsViewP          :: Camera.ViewProcess
  , rsCameraControls :: CameraControls.ControlsProcess

  , rsSceneP    :: Set0.Process

  , rsNetwork         :: EventNetwork
  , rsNetworkActuated :: Worker.Var Bool

    -- Unidirectional components
  , rsFromNetwork :: Worker.Cell Double Text
  , rsToNetwork   :: Worker.Var Text

    -- Bidirectional component
  , rsMesh       :: Mesh.Model             -- static data
  , rsMeshInput  :: StateVar Mesh.Input    -- bidirectional input
  , rsMeshOutput :: Worker.Var Mesh.Output -- observable output

  , rsFrameEvent :: () -> IO ()
  , rsFPS        :: Worker.Var Float

  , rsCursorPos   :: Worker.Var (Double, Double)
  , rsPlanePos    :: Worker.Var (Maybe Vec3)
  , rsPlaneCursor :: Worker.Var (Storable.Vector Transform)
  }
