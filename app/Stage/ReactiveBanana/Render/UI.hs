module Stage.ReactiveBanana.Render.UI
  ( imguiDrawData
  ) where

import RIO.Local

import DearImGui qualified
import Engine.Types (StageFrameRIO)
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.ImGui qualified as ImGui
import Reactive.Banana.Frameworks qualified as RBF

import Stage.ReactiveBanana.Types (FrameResources(..), RunState(..))

type DrawM = StageFrameRIO Basic.RenderPasses Basic.Pipelines FrameResources RunState

imguiDrawData :: DrawM DearImGui.DrawData
imguiDrawData = do
  fromNetwork <- gets rsFromNetwork >>= Worker.getOutputData

  toNetwork <- gets rsToNetwork

  network <- gets rsNetwork
  networkActuated <- gets rsNetworkActuated

  meshInput <- gets rsMeshInput

  fps <- gets rsFPS >>= Worker.getOutputData

  cursorPos <- gets rsCursorPos >>= Worker.getOutputData
  planePos <- gets rsPlanePos >>= Worker.getOutputData

  fmap snd $ ImGui.mkDrawData do
    DearImGui.withWindowOpen "Keid + DearImGui + reactive-banana" do
      DearImGui.checkbox "Actuated" (Worker.stateVar networkActuated) >>= \changed ->
        when changed do
          actuate <- Worker.getOutputData networkActuated
          liftIO $
            if actuate then
              RBF.actuate network
            else
              RBF.pause network

      DearImGui.text . fromString $
        "From network: " <> show fromNetwork

      changed <- DearImGui.inputTextWithHint
        "To network"
        ""
        (Worker.stateVar toNetwork)
        50
      when changed $
        logInfo "DearImGui: Input changed"

      DearImGui.separator

      void $! DearImGui.sliderInt
        "Cubes"
        meshInput
        0
        16

      DearImGui.text . fromString $ "FPS: " <> show fps
      DearImGui.text . fromString $ "Window CursorPos: " <> show cursorPos
      DearImGui.text . fromString $ "Scene PlanePos: " <> show planePos
