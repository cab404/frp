module Stage.ReactiveBanana.Events
  ( spawn
  ) where

import RIO.Local

import Engine.Camera.Event.Handler qualified as CameraHandler
import Engine.Camera.Event.Type qualified as Camera
import Engine.Events qualified as Events
import Engine.Events.MouseButton qualified as MouseButton
import Engine.Types (StageRIO)
import Engine.Window.Key (Key(..), KeyState(..))
import Engine.Window.Key qualified as Key
import Engine.Window.Scroll qualified as Scroll
import Engine.Worker qualified as Worker
import Render.ImGui qualified as ImGui
import UnliftIO.Resource (ReleaseKey)

import Stage.ReactiveBanana.Event.Type (Event(..))
import Stage.ReactiveBanana.Types (RunState(..))

spawn :: StageRIO RunState (ReleaseKey, Events.Sink Event RunState)
spawn = do
  dummyCursor <- Worker.newVar 0
  Events.spawn handleEvent
    [ Key.callback . keyHandler
    , Scroll.callback . scrollHandler
      -- XXX: gets overwritten by EventNetwork handlers
    , MouseButton.callback dummyCursor clickHandler
    ]

handleEvent :: Event -> StageRIO RunState ()
handleEvent = \case
  DoNothing ->
    pure ()

  Camera event ->
    CameraHandler.handler
      (gets rsViewP)
      (gets rsCameraControls)
      event

-- _invalidateShadowMap :: StageRIO RunState ()
-- _invalidateShadowMap = do
--   shadowVar <- gets rsUpdateShadow
--   Worker.pushInput shadowVar id

clickHandler :: MouseButton.ClickHandler Event RunState
clickHandler (Events.Sink _signal) _cursorPos mouseEvent = ImGui.capturingMouse do
  case mouseEvent of
    (_mods, _state, btn) ->
      logDebug $ "Mouse button: " <> displayShow btn

scrollHandler :: Events.Sink Event RunState -> Double -> Double -> StageRIO RunState ()
scrollHandler (Events.Sink signal) dx dy = ImGui.capturingMouse do
  logDebug $ "Scroll: " <> displayShow (dx, dy)
  when (dy /= 0) do
    signal . Camera $ Camera.Zoom (double2Float dy)

keyHandler :: Events.Sink Event RunState -> Key.Callback RunState
keyHandler (Events.Sink signal) _keyCode keyEvent = ImGui.capturingKeyboard do
  let (_mods, keyState, key) = keyEvent
  -- logDebug $ "Key event (" <> display keyCode <> "): " <> displayShow keyEvent

  case (keyState, key) of
    (KeyState'Pressed, Key'Right) ->
      signal . Camera $ Camera.PanHorizontal (-1)
    (KeyState'Released, Key'Right) ->
      signal . Camera $ Camera.PanHorizontal 0

    (KeyState'Pressed, Key'Left) ->
      signal . Camera $ Camera.PanHorizontal 1
    (KeyState'Released, Key'Left) ->
      signal . Camera $ Camera.PanHorizontal 0

    (KeyState'Pressed, Key'Up) ->
      signal . Camera $ Camera.PanVertical (-1)
    (KeyState'Released, Key'Up) ->
      signal . Camera $ Camera.PanVertical 0

    (KeyState'Pressed, Key'Down) ->
      signal . Camera $ Camera.PanVertical 1
    (KeyState'Released, Key'Down) ->
      signal . Camera $ Camera.PanVertical 0

    (KeyState'Pressed, Key'Insert) ->
      signal . Camera $ Camera.TurnAzimuth (-1)
    (KeyState'Released, Key'Insert) ->
      signal . Camera $ Camera.TurnAzimuth 0

    (KeyState'Pressed, Key'Delete) ->
      signal . Camera $ Camera.TurnAzimuth 1
    (KeyState'Released, Key'Delete) ->
      signal . Camera $ Camera.TurnAzimuth 0

    (KeyState'Pressed, Key'Home) ->
      signal . Camera $ Camera.TurnInclination 1
    (KeyState'Released, Key'Home) ->
      signal . Camera $ Camera.TurnInclination 0

    (KeyState'Pressed, Key'End) ->
      signal . Camera $ Camera.TurnInclination (-1)
    (KeyState'Released, Key'End) ->
      signal . Camera $ Camera.TurnInclination 0

    _ ->
      pure ()
