{-# LANGUAGE OverloadedLists #-}

module Stage.ReactiveBanana.Setup
  ( stackStage
  -- , Options(..)
  ) where

import RIO.Local

import Engine.Camera qualified as Camera
import Engine.Camera.Controls qualified as CameraControls
import Engine.Stage.Component qualified as Stage
import Engine.Types (StackStage(..), StageRIO)
import Engine.Types qualified as Engine
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.ImGui qualified as ImGui
import Resource.Buffer qualified as Buffer
import Resource.CommandBuffer (withPools)
import Resource.Region qualified as Region
import RIO.State (modify')
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Stage.ReactiveBanana.Events qualified as Events
import Stage.ReactiveBanana.Network.Stuff qualified as StuffNetwork
import Stage.ReactiveBanana.Network.Window qualified as WindowNetwork
import Stage.ReactiveBanana.Render qualified as Render
import Stage.ReactiveBanana.Types (FrameResources(..), RunState(..), Stage)
import Stage.ReactiveBanana.World.Mesh qualified as Mesh
import Stage.ReactiveBanana.World.Scene qualified as Scene

stackStage :: StackStage
stackStage = StackStage stage

stage :: Stage
stage = Stage.assemble "ReactiveBanana" rendering resources (Just mainScene)

rendering :: Basic.Rendering st
rendering = ImGui.renderWith Basic.rpForwardMsaa 0 Basic.rendering_

resources :: Stage.Resources Basic.RenderPasses Basic.Pipelines RunState FrameResources
resources = Stage.Resources
  { rInitialRS = initialRunState
  , rInitialRR = initialFrameResources
  }

initialRunState :: StageRIO env (Resource.ReleaseKey, RunState)
initialRunState = Region.run $ withPools \pools -> do
  perspective <- Region.local . Worker.registered $
    Camera.spawnPerspective

  rsViewP <- Region.local . Worker.registered $
    CameraControls.spawnViewOrbital Camera.initialOrbitalInput
      { Camera.orbitAzimuth = τ / 2
      , Camera.orbitAscent  = -τ / 4.01
      }

  rsCameraControls <- lift $ CameraControls.spawnControls rsViewP

  Region.local_ $ Worker.registerCollection rsCameraControls

  -- sceneInput <- Worker.newVar Scene.initialInput
  rsSceneP <- Region.local . Worker.registered $
    Worker.spawnMerge3 Scene.mkScene perspective rsViewP (Worker.getInput perspective)

  networkStuff <- StuffNetwork.allocate
  let
    ( rsFromNetwork
      , rsToNetwork
      , rsNetworkActuated
      , rsMeshInput
      , rsMeshOutput
      , rsFrameEvent
      , rsFPS
      , rsCursorPos
      , rsPlanePos
      , rsPlaneCursor
      , rsNetwork
      ) = networkStuff

  context <- ask
  rsMesh <- Region.local $
    Mesh.mkModel context pools

  let rsEvents = Nothing
  pure RunState{..}

initialFrameResources
  :: Queues Vk.CommandPool
  -> Basic.RenderPasses
  -> Basic.Pipelines
  -> ResourceT (Engine.StageRIO RunState) FrameResources
initialFrameResources _pools _passes pipelines = do
  -- context <- ask

  frScene <- Set0.allocateEmpty (Basic.getSceneLayout pipelines)

  frMeshInstances <- Buffer.newObserverCoherent Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 4

  frPlaneCursor <- Buffer.newObserverCoherent Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 1

  pure FrameResources{..}

mainScene :: Stage.Scene Basic.RenderPasses Basic.Pipelines RunState FrameResources
mainScene = Stage.Scene
  { scBeforeLoop = do
      sink <- Region.local Events.spawn
      modify' \ rs -> rs
        { rsEvents = Just sink
        }
      _windowEvents <- WindowNetwork.allocate
      ImGui.allocateLoop True
  , scUpdateBuffers = Render.updateBuffers
  , scRecordCommands = Render.recordCommands
  }
