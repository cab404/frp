{-# LANGUAGE OverloadedLists #-}

module Stage.ReactiveBanana.Render
  ( updateBuffers
  , recordCommands
  ) where

import RIO.Local

import Engine.Types qualified as Engine
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Worker qualified as Worker
import Render.Basic (RenderPasses(..), Pipelines, PipelinesF(..))
import Render.DescSets.Set0 qualified as Set0
import Render.Draw qualified as Draw
import Render.ForwardMsaa qualified as ForwardMsaa
import Render.ImGui qualified as ImGui
import Resource.Buffer qualified as Buffer
import Vulkan.Core10 qualified as Vk

import Stage.ReactiveBanana.Render.UI (imguiDrawData)
import Stage.ReactiveBanana.Types (FrameResources(..), RunState(..))

updateBuffers
  :: RunState
  -> FrameResources
  -> Engine.StageFrameRIO RenderPasses Pipelines FrameResources RunState ()
updateBuffers RunState{..} FrameResources{..} = do
  liftIO $ rsFrameEvent ()
  Set0.observe rsSceneP frScene
  Buffer.observeCoherentResize_ rsMeshOutput frMeshInstances
  Buffer.observeCoherentResize_ rsPlaneCursor frPlaneCursor

recordCommands
  :: Vk.CommandBuffer
  -> FrameResources
  -> Word32
  -> Engine.StageFrameRIO RenderPasses Pipelines FrameResources RunState ()
recordCommands cb _fr@FrameResources{..} imageIndex = do
  (_context, Engine.Frame{fSwapchainResources, fRenderpass, fPipelines}) <- ask

  -- sceneDrawCasters <- prepareCasters fr
  -- (sceneDepthPrepass, sceneDrawOpaque, sceneDrawBlended) <- prepareScene fPipelines fr
  dear <- imguiDrawData

  mesh <- gets rsMesh
  meshInstances <- Worker.readObservedIO frMeshInstances
  planeCursor <- Worker.readObservedIO frPlaneCursor

  ForwardMsaa.usePass (rpForwardMsaa fRenderpass) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    Set0.withBoundSet0 frScene (pWireframe fPipelines) cb do
      Graphics.bind cb (pWireframe fPipelines) do
        Draw.indexed cb mesh meshInstances
        Draw.indexed cb mesh planeCursor

    ImGui.draw dear cb
