module Main (main) where

import RIO

import Engine.App (engineMain)

import Stage.ReactiveBanana.Setup (stackStage)

main :: IO ()
main = engineMain stackStage
